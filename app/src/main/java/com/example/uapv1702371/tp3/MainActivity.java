package com.example.uapv1702371.tp3;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {
            class CityList  extends BaseAdapter {
        List<City> dataSource;

        public CityList(List<City> data) {
            //super(MainActivity.this, R.layout.row, data);
            dataSource=data;
        }
        public void setData(List<City> d)
        {
            dataSource=d;
        }
        @Override
        public int getCount() {
            return dataSource.size();
        }
        @Override
        public Object getItem(int position) {
            return dataSource.get(position);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        public void updateCityList(List<City> newlist) {
                    dataSource.clear();
                    dataSource.addAll(newlist);
                    this.notifyDataSetChanged();
                }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            City c = dataSource.get(position);
            convertView = LayoutInflater.from(MainActivity.this).inflate(R.layout.row,null);

            TextView CityName = (TextView) convertView.findViewById(R.id.cName);
            TextView CityCountry = (TextView) convertView.findViewById(R.id.cCountry);
            TextView CityTemp= (TextView) convertView.findViewById(R.id.temperature);
            ImageView CityImage = (ImageView) convertView.findViewById(R.id.imageViewRow);

            CityName.setText(c.getName());
            CityCountry.setText(c.getCountry());
            if(c.getIcon()!=null)
            {
                Resources resources = MainActivity.this.getResources();
                String my_icon="icon_"+c.getIcon();
                final int resourceId = getResources().getIdentifier(my_icon, "drawable",
                        MainActivity.this.getPackageName());
                CityImage.setImageDrawable(getResources().getDrawable(resourceId));
            }
            String temp=c.getTemperature();
            if(temp==null)
            {
                CityTemp.setText(c.getTemperature());
            }
            return convertView;
        }
    }
    CityList CL=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        final Intent intent10 = new Intent(this, NewCityActivity.class);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                startActivityForResult(intent10,2);
            }
        });

        WeatherDbHelper dbHelper = new WeatherDbHelper(this);
        CL = new CityList(dbHelper.getAllCities());

        ListView list = (ListView) findViewById(R.id.listv);
        list.setAdapter(CL);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id)
            {

                final Intent intent = new Intent(getApplicationContext(), CityActivity.class);
                final City item = (City) parent.getItemAtPosition(position);
                intent.putExtra(City.TAG, item );
                startActivityForResult(intent,1);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == 1)
        {

        }
        else if(requestCode == 2 )
        {
            WeatherDbHelper dbHelper = new WeatherDbHelper(this);
            dbHelper.addCity((City) data.getParcelableExtra("City"));
            CL.updateCityList(dbHelper.getAllCities());
        }

       /* if(requestCode == 1)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                final Book B=(Book) data.getParcelableExtra("book2");
                BH.updateBook(B);
                finish();
                startActivity(getIntent());
            }
        }
        else  if(requestCode == 2)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                final Book B=(Book) data.getParcelableExtra("book2");
                BH.addBook(B);
                finish();
                startActivity(getIntent());

            }
        }*/

    }
}
