package com.example.uapv1702371.tp3;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

//import fr.uavignon.shuet.tp3.data.City;

public class NewCityActivity extends AppCompatActivity {

    private EditText textName, textCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);

        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                TextView CityName = (TextView) findViewById(R.id.editNewName);
                TextView CityCountry = (TextView) findViewById(R.id.editNewCountry);

                final Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                City item = new City(CityName.getText().toString(),CityCountry.getText().toString());
                intent.putExtra(City.TAG, item );
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
    }
}
